# language: es

Característica: Promocion Categoria

Antecedentes:
  Dado el producto "Gaseosa" que cuesta 10 y pertenece a la categoria "Bebidas"
  Y el producto "Hamburguesa" que cuesta 20 y pertenece a la categoria "Congelados"
  Y el producto "Cereal" que cuesta 5 y pertenece a la categoria "Desayuno"

Escenario: ECAT1 - Cuando compro un producto de la Categoria 'Bebidas', obtengo el descuento 
  Dado la promo DescuentoCategoria para "Bebidas" con 20 %
  Cuando compro 1 unidad del producto "Gaseosa"
  Y pago con "efectivo"
  Entonces el monto a pagar es 8.0

Escenario: ECAT2 - Cuando No compro un producto de la Categoria 'Bebidas', No obtengo el descuento 
  Dado la promo DescuentoCategoria para "Bebidas" con 20 %
  Cuando compro 1 unidad del producto "Hamburguesa"
  Y pago con "efectivo"
  Entonces el monto a pagar es 20.0

Escenario: ECAT3 - Cuando compro varios productos de la categoria 'Congelados', obtengo el descuento en todos los productos
  Dado la promo DescuentoCategoria para "Congelados" con 20 %
  Cuando compro 2 unidad del producto "Hamburguesa"
  Y pago con "efectivo"
  Entonces el monto a pagar es 32.0
