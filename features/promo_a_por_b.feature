# language: es
Característica: Promocion AxB

Antecedentes:
  Dado el producto "Gaseosa" que cuesta 10
  Y el producto "Hamburguesa" que cuesta 20
  Y el producto "Cereal" que cuesta 5

Escenario: EAB1 - Cuando no compro dos productos la promo no aplica
  Dado la promo APorB para el producto "Gaseosa" y "Hamburguesa"
  Cuando compro 1 unidad del producto "Cereal"
  Y pago con "efectivo"
  Entonces el monto a pagar es 5.0

Escenario: EAB2 - Cuando los productos de la promo el segundo les gratis
  Dado la promo APorB para el producto "Gaseosa" y "Hamburguesa"
  Cuando compro 1 unidad del producto "Gaseosa"
  Cuando compro 1 unidad del producto "Hamburguesa"
  Y pago con "efectivo"
  Entonces el monto a pagar es 10.0