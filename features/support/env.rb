require 'faraday'
require 'json'
require 'rspec/expectations'
require 'rspec'
require 'rack/test'
require 'rspec/expectations'
require 'byebug'
require 'sinatra'
require_relative '../../app.rb'

RACK_ENV='test'
BASE_URL = 'http://localhost:4567'
if ENV['BASE_URL']
  BASE_URL = ENV['BASE_URL']
else
  include Rack::Test::Methods

  def app
    Sinatra::Application
  end
end


PRODUCT_URL = BASE_URL + '/product'
PROMO_URL = BASE_URL + '/promo'
CART_URL = BASE_URL + '/cart'
PAY_URL = BASE_URL + '/pay'
DATE_URL = BASE_URL + '/date'
DESCUENTO_2_UNIDAD = '2u'
DESCUENTO_2_POR_1 = '2x1'
DESCUENTO_TARJETA = 'tarjeta'
DESCUENTO_EFECTIVO = 'efectivo'
DESCUENTO_FIN_DE_SEMANA = 'finde'
DESCUENTO_CATEGORIA = 'categoria'
DESCUENTO_A_POR_B='aporb'
