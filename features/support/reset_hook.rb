After do |scenario|
  reset_url = BASE_URL + '/reset'
  response = Faraday.post(reset_url, '')
  expect(response.status).to eq(200)
end