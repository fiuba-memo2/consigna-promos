Cuando("creo otro producto {string}") do |producto|
  precio_que_no_importa = 10
  @request = {
    "nombre": producto,
    "precio": precio_que_no_importa
  }
  @response = Faraday.post(PRODUCT_URL, @request.to_json, "Content-Type" => "application/json")
end

Entonces("entonces obtengo error") do
  expect(@response.status).to eq 400
end