Dado("el producto {string} que cuesta {int}") do |producto, precio|
  @request = {
    "nombre": producto,
    "precio": precio
  }
  @response = Faraday.post(PRODUCT_URL, @request.to_json, "Content-Type" => "application/json")
end

Dado("el producto {string} que cuesta {int} y pertenece a la categoria {string}") do |producto, precio, categoria|
  @request = {
    "nombre": producto,
    "precio": precio,
    "categoria": categoria
  }
  @response = Faraday.post(PRODUCT_URL, @request.to_json, "Content-Type" => "application/json")
end

Dado("la promo DosPorUno para el producto {string}") do |producto|
  @request = {
    "nombre": DESCUENTO_2_POR_1,
    "producto": producto
  }
  @response = Faraday.post(PROMO_URL, @request.to_json, "Content-Type" => "application/json")
end

Dado("la promo APorB para el producto {string} y {string}") do |productoA, productoB|
  @request = {
    "nombre": DESCUENTO_A_POR_B,
    "producto": productoA,
    "producto2": productoB
  }
  @response = Faraday.post(PROMO_URL, @request.to_json, "Content-Type" => "application/json")
end

Dado("la promo SegundaUnidad para el producto {string} con {int} %") do |producto, descuento|
  @request = {
    "nombre": DESCUENTO_2_UNIDAD,
    "producto": producto,
    "descuento": descuento
  }
  @response = Faraday.post(PROMO_URL, @request.to_json, "Content-Type" => "application/json")
end

Dado("la promo Tarjeta para el producto {string} con {int} %") do |producto, descuento|
  @request = {
    "nombre": DESCUENTO_TARJETA,
    "producto": producto,
    "descuento": descuento
  }
  @response = Faraday.post(PROMO_URL, @request.to_json, "Content-Type" => "application/json")
end

Dado("la promo Efectivo con {int} %") do |descuento|
  @request = {
    "nombre": DESCUENTO_EFECTIVO,
    "descuento": descuento
  }
  @response = Faraday.post(PROMO_URL, @request.to_json, "Content-Type" => "application/json")
end

Dado("la promo DescuentoCategoria para {string} con {int} %") do |categoria, descuento|
  @request = {
    "nombre": DESCUENTO_CATEGORIA,
    "categoria": categoria,
    "descuento": descuento
  }
  @response = Faraday.post(PROMO_URL, @request.to_json, "Content-Type" => "application/json")
end

Dado("la promo FinDeSemana para el producto {string} con {int} %") do |producto, descuento|
  @request = {
    "nombre": DESCUENTO_FIN_DE_SEMANA,
    "producto": producto,
    "descuento": descuento
  }
  @response = Faraday.post(PROMO_URL, @request.to_json, "Content-Type" => "application/json")
end

Cuando("es {string}") do |fecha|
  @request = {
    "fecha": "#{fecha}"
  }
  @response = Faraday.post(DATE_URL, @request.to_json, "Content-Type" => "application/json")
end

Cuando("compro {int} unidad(es) del producto {string}") do |cantidad, producto|
  @request = {
    "producto": producto,
    "cantidad": cantidad
    }
    @response = Faraday.post(CART_URL, @request.to_json, "Content-Type" => "application/json")          
end
  
Y("pago con {string}") do |payment_mode|
  @request = {
    "payment": payment_mode,
  }
  @response = Faraday.post(PAY_URL, @request.to_json, "Content-Type" => "application/json")      
end
  
Entonces("el monto a pagar es {float}") do |amount_to_pay|
  json_response = JSON.parse(@response.body)
  expect(json_response['amount_to_pay']).to eq(amount_to_pay)
end