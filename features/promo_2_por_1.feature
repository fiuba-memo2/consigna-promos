# language: es
Característica: Promocion 2x1

Antecedentes:
  Dado el producto "Gaseosa" que cuesta 10
  Y el producto "Hamburguesa" que cuesta 20
  Y el producto "Cereal" que cuesta 5

Escenario: E1 - Cuando no compro dos productos la promo no aplica
  Dado la promo DosPorUno para el producto "Gaseosa"
  Cuando compro 1 unidad del producto "Cereal"
  Y pago con "efectivo"
  Entonces el monto a pagar es 5.0

Escenario: E2 - Cuando un producto de la promo la promo no aplica
  Dado la promo DosPorUno para el producto "Gaseosa"
  Cuando compro 1 unidades del producto "Gaseosa"
  Y pago con "efectivo"
  Entonces el monto a pagar es 10.0

Escenario: E3 - Cuando dos productos de la promo pago 1
  Dado la promo DosPorUno para el producto "Gaseosa"
  Cuando compro 2 unidades del producto "Gaseosa"
  Y pago con "efectivo"
  Entonces el monto a pagar es 10.0

Escenario: E4 - La Promo aplica independiente del modo de pago
  Dado la promo DosPorUno para el producto "Gaseosa"
  Cuando compro 2 unidades del producto "Gaseosa"
  Y pago con "tarjeta"
  Entonces el monto a pagar es 10.0

Escenario: E5 - La Promo aplica solamente sobre los producto de la promo
  Dado la promo DosPorUno para el producto "Gaseosa"
  Cuando compro 2 unidades del producto "Gaseosa"
  Y compro 2 unidades del producto "Hamburguesa"
  Y compro 2 unidades del producto "Cereal"
  Y pago con "tarjeta"
  Entonces el monto a pagar es 60.0
