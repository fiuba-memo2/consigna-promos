# language: es
Característica: Promocion Efectivo

Antecedentes:
  Dado el producto "Gaseosa" que cuesta 10
  Y el producto "Hamburguesa" que cuesta 20
  Y el producto "Cereal" que cuesta 5

Escenario: EE1 - Cuando compro con la promo Efectivo y pago con efectivo obtengo descuento
  Dado la promo Efectivo con 10 %
  Cuando compro 1 unidad del producto "Gaseosa"
  Y pago con "efectivo"
  Entonces el monto a pagar es 9.0

Escenario: EE2 - Cuando compro un producto que tiene promo Efectivo y pago con tarjeta No obtengo el descuento
  Dado la promo Efectivo con 10 %
  Cuando compro 1 unidad del producto "Hamburguesa"
  Y pago con "tarjeta"
  Entonces el monto a pagar es 20.0

Escenario: EE3 - Cuando compro con la promo Efectivo y la promo 2x1 obtengo ambos descuentos
  Dado la promo Efectivo con 10 %
  Dado la promo DosPorUno para el producto "Gaseosa"
  Cuando compro 2 unidad del producto "Gaseosa"
  Y pago con "efectivo"
  Entonces el monto a pagar es 9.0
  