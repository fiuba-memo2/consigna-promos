# language: es
Característica: Colision promos

Antecedentes:
  Dado el producto "Gaseosa" que cuesta 10 y pertenece a la categoria "Bebidas"
  Y el producto "Hamburguesa" que cuesta 20
  Y el producto "Cereal" que cuesta 5
  Y el producto "Agua" que cuesta 8 y pertenece a la categoria "Bebidas"

Escenario: EC1 - Cuando tengo varias promos que aplican sobre el mismo producto solo aplica la primera
  Dado la promo Tarjeta para el producto "Gaseosa" con 70 %
  Dado la promo DosPorUno para el producto "Gaseosa"
  Cuando compro 2 unidad del producto "Gaseosa"
  Y pago con "tarjeta"
  Entonces el monto a pagar es 6.0


Escenario: EC2 - Cuando tengo promo tarjetas y 2x1 y pago con efectivo aplica 2x1
  Dado la promo Tarjeta para el producto "Gaseosa" con 70 %
  Dado la promo DosPorUno para el producto "Gaseosa"
  Cuando compro 2 unidad del producto "Gaseosa"
  Y pago con "efectivo"
  Entonces el monto a pagar es 10.0

Escenario: EC3 - Cuando tengo promo tarjetas y 2x1 y pago con tarjeta aplica tarjeta
  Dado la promo Tarjeta para el producto "Gaseosa" con 70 %
  Dado la promo DosPorUno para el producto "Gaseosa"
  Cuando compro 3 unidad del producto "Gaseosa"
  Y pago con "tarjeta"
  Entonces el monto a pagar es 9.0

Escenario: EC4 - Cuando tengo promo 2x1 y tarjeta y pago con tarjeta aplica 2x1
  Dado la promo DosPorUno para el producto "Gaseosa"
  Dado la promo Tarjeta para el producto "Gaseosa" con 70 %
  Cuando compro 3 unidad del producto "Gaseosa"
  Y pago con "tarjeta"
  Entonces el monto a pagar es 20.0


Escenario: EC5 - Cuando tengo dos promos del mismo sobre el mismo producto aplica la primera
  Dado la promo Tarjeta para el producto "Gaseosa" con 70 %
  Dado la promo DosPorUno para el producto "Gaseosa"
  Cuando compro 2 unidad del producto "Gaseosa"
  Y pago con "tarjeta"
  Entonces el monto a pagar es 6.0

Escenario: EC6 - Colision en uno de los productos
  Dado la promo DosPorUno para el producto "Agua"
  Y la promo DescuentoCategoria para "Bebidas" con 20 %
  Cuando compro 1 unidad del producto "Gaseosa"
  Y compro 2 unidades del producto "Agua"
  Y pago con "efectivo"
  Entonces el monto a pagar es 16.0  
