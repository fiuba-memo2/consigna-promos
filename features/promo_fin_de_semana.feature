# language: es
Característica: Promocion descuento de fin de semana

Antecedentes:
  Dado el producto "Gaseosa" que cuesta 10
  Y el producto "Hamburguesa" que cuesta 20
  Y el producto "Cereal" que cuesta 5

Escenario: EF1 - Cuando compro un sabado y tengo promo fin de semana tengo descuento
  Dado la promo FinDeSemana para el producto "Gaseosa" con 30 %
  Cuando compro 1 unidad del producto "Gaseosa"
  Y es "2020-05-02"
  Y pago con "efectivo"
  Entonces el monto a pagar es 7.0

Escenario: EF2 - Cuando compro un domingo y tengo promo fin de semana tengo descuento
  Dado la promo FinDeSemana para el producto "Gaseosa" con 30 %
  Cuando compro 1 unidad del producto "Gaseosa"
  Y es "2020-05-03"
  Y pago con "efectivo"
  Entonces el monto a pagar es 7.0

Escenario: EF3 - Cuando compro un lunes y tengo promo fin de semana no tengo descuento
  Dado la promo FinDeSemana para el producto "Gaseosa" con 30 %
  Cuando compro 1 unidad del producto "Gaseosa"
  Y es "2020-05-04"
  Y pago con "efectivo"
  Entonces el monto a pagar es 10.0

Escenario: EF4 - Cuando compro un sabado y tengo promo fin de semana pero no compro productos de la promo no tengo descuento
  Dado la promo FinDeSemana para el producto "Gaseosa" con 30 %
  Cuando compro 1 unidad del producto "Cereal"
  Y es "2020-05-02"
  Y pago con "efectivo"
  Entonces el monto a pagar es 5.0