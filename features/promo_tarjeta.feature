# language: es
Característica: Promocion tarjeta

Antecedentes:
  Dado el producto "Gaseosa" que cuesta 10
  Y el producto "Hamburguesa" que cuesta 20
  Y el producto "Cereal" que cuesta 5

Escenario: ET1 - Cuando compro producto que tiene promo tarjeta y pago con tarjeta obtengo descuento
  Dado la promo Tarjeta para el producto "Gaseosa" con 70 %
  Cuando compro 1 unidad del producto "Gaseosa"
  Y pago con "tarjeta"
  Entonces el monto a pagar es 3.0

Escenario: ET2 - Cuando compro producto que tiene promo tarjeta y pago con efectivo No obtengo descuento
  Dado la promo Tarjeta para el producto "Gaseosa" con 70 %
  Cuando compro 1 unidades del producto "Gaseosa"
  Y pago con "efectivo"
  Entonces el monto a pagar es 10.0

Escenario: ET3 - Cuando tengo promo tarjeta sobre un producto y compro otro producto y pago con tarjeta no tengo descuento
  Dado la promo Tarjeta para el producto "Gaseosa" con 70 %
  Cuando compro 1 unidades del producto "Cereal"
  Y pago con "tarjeta"
  Entonces el monto a pagar es 5.0