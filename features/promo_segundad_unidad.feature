# language: es
Característica: Promocion segunda unidad

Antecedentes:
  Dado el producto "Gaseosa" que cuesta 10
  Y el producto "Hamburguesa" que cuesta 20
  Y el producto "Cereal" que cuesta 5

Escenario: ESU1 - Cuando compro una sola unidad y tiene promo segunda unidad la promo no aplica
  Dado la promo SegundaUnidad para el producto "Gaseosa" con 70 %
  Cuando compro 1 unidad del producto "Gaseosa"
  Y pago con "efectivo"
  Entonces el monto a pagar es 10.0

Escenario: ESU2 - Cuando compro dos unidades y tiene promo segunda unidad la promo aplica
  Dado la promo SegundaUnidad para el producto "Gaseosa" con 70 %
  Cuando compro 2 unidades del producto "Gaseosa"
  Y pago con "efectivo"
  Entonces el monto a pagar es 13.0

Escenario: ESU3 - sssss
  Dado la promo SegundaUnidad para el producto "Gaseosa" con 70 %
  Cuando compro 3 unidades del producto "Gaseosa"
  Y pago con "efectivo"
  Entonces el monto a pagar es 23.0  

Escenario: ESU4 - sxxxx
  Dado la promo SegundaUnidad para el producto "Gaseosa" con 70 %
  Cuando compro 4 unidades del producto "Gaseosa"
  Y pago con "efectivo"
  Entonces el monto a pagar es 26.0    