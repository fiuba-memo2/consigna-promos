# language: es
Característica: Altas

Escenario: AP1 - Cuando doy de alta un producto repetido obtengo error
  Dado el producto "Gaseosa" que cuesta 10
  Cuando creo otro producto "Gaseosa"
  Entonces entonces obtengo error

Escenario: AP2 - Cuando doy de alta una promo con descuento negativo obtengo error
  Cuando la promo Tarjeta para el producto "Gaseosa" con -1 %
  Entonces entonces obtengo error

Escenario: AP3 - Cuando doy de alta un producto con precio negativo obtengo error
  Cuando el producto "Gaseosa" que cuesta -10
  Entonces entonces obtengo error  

Escenario: AP4 - Cuando doy de alta una promo del mismo tipo sobre el mismo producto obtengo error
  Cuando la promo Tarjeta para el producto "Gaseosa" con 10 %
  Y la promo Tarjeta para el producto "Gaseosa" con 20 %
  Entonces entonces obtengo error
