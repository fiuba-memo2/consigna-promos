# promos

Se pide hacer una aplicación (API Rest) para calcular el monto a pagar en una compra de supermercado considerando distintos tipos de descuentos/promociones.

La aplicación debe proveer 4 endpoints:

* POST alta producto (/product)
* POST alta promo (/promo)
* POST agregar al carrito (/cart)
* POST checkout (/pay)

Adicionalmente para propósitos de testing se requiere un endpoint para resetear el estado de la aplicación y otro para setear la fecha:

* POST /reset
* POST /date


Promociones

* Promo 2x1
* Promo Segunda Unidad
* Promo tarjeta
* Promo Fin de Semana